<?php

namespace App\Repositories;

use App\Core\Repositories\BaseRepository;
use App\Product;
use App\Exceptions\ProductNotFoundException;

class ProductRepository extends BaseRepository
{
    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     * @throws ProductNotFoundException
     */
    public function getAllProducts()
    {
        $products = Product::all();
        if (!count($products))
            throw new ProductNotFoundException('No Product', 404);
        return $products;
    }

    /**
     * @param $id
     * @throws ProductNotFoundException
     */

    public function getProductById($id)
    {
        $product = Product::find($id);
        if (!$product->count())
            throw new ProductNotFoundException('Product not found', 404);
        return $product;
    }

    /**
     * @param $data
     * @return $this|\Illuminate\Database\Eloquent\Model
     */

    public function createProduct($data)
    {
        return Product::create([
            'name'=>$data['name'],
            'price'=>$data['price']
        ]);

    }

}