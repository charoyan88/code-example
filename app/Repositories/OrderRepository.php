<?php

namespace App\Repositories;

namespace App\Repositories;

use App\Core\Repositories\BaseRepository;
use App\Order;
use App\Exceptions\OrderNotFoundException;


class OrderRepository extends BaseRepository
{
    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     * @throws OrderNotFoundException
     */
    public function getAllOrders()
    {
        $orders = Order::all();
        if (!count($orders))
            throw new OrderNotFoundException('No Orders', 404);
        return $orders;
    }

    /**
     * @param $id
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null|static|static[]
     * @throws OrderNotFoundException
     */
    public function getOrderById($id)
    {
        $order = Order::find($id);
        if (!count($order))
            throw new OrderNotFoundException('Order not found', 404);
        return $order;

    }

    /**
     * @param $data
     * @return $this|\Illuminate\Database\Eloquent\Model
     */

    public function createOrder($data)
    {
        return Order::create([
            'customers_id' => $data['customers_id']
        ]);
    }

}