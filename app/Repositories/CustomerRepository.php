<?php

namespace App\Repositories;


use App\Core\Repositories\BaseRepository;
use App\Exceptions\CustomerNotFoundException;
use App\Customer;


class CustomerRepository extends BaseRepository
{

    /**
     * @param $data
     * @return $this|\Illuminate\Database\Eloquent\Model
     */
    public function createCustomer($data)
    {
        return Customer::create([
            'name'=>$data['name'],
            'phone_number'=>$data['phone_number'],
        ]);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     * @throws CustomerNotFoundException
     */

    public function getAllCustomers()
    {
        $customers = Customer::all();
        if(!count($customers))
            throw new CustomerNotFoundException('No Customer', 404);
        return $customers;
    }

    /**
     * @param $id
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null|static|static[]
     * @throws CustomerNotFoundException
     */

    public function getCustomerById($id)
    {
        $customer = Customer::find($id);
        if(!count($customer))
            throw new CustomerNotFoundException('Customer not found', 404);
        return $customer;
    }

}