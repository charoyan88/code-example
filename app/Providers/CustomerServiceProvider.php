<?php

namespace App\Providers;

use App\Contracts\ClientServiceInterface;
use App\Services\ClientService;
use Illuminate\Support\ServiceProvider;

class CustomerServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(CustomerServiceInterface::class,CustomerService::class);
    }
}