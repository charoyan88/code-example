<?php

namespace App\Providers;


use App\Contracts\OrderItemServiceInterface;
use App\Services\OrderItemService;
use Illuminate\Support\ServiceProvider;

class OrderItemServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(OrderItemServiceInterface::class,OrderItemService::class);
    }
}