<?php

namespace App\Http\Controllers;

use App\Services\CustomerService;
use App\Services\OrderService;
use Illuminate\Http\Request;

class OrderContorller extends Controller
{
    /**
     * @param Request $request
     * @param OrderService $orderService
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request, OrderService $orderService)
    {
        try {
            $this->validate($request, [
                'customers_id' => 'required:orders',
                'products_id' => 'required:orders'
            ]);

            $data = $request->all();

            $order = $orderService->createCustomer($data);
            $response = ['data' => $order, 'success' => true, 'error' => false, 'message' => 'Customer successfully created!'];
            $status = 201;
        } catch (\Exception $exception) {
            $response = ['data' => [], 'success' => false, 'error' => true, 'message' => $exception->getMessage()];
            $status = !empty($exception->status) ? $exception->status : 200;
        }
        return response()->json($response, $status);
    }

    /**
     * @param OrderService $orderService
     * @return \Illuminate\Http\JsonResponse
     */
    public function getOrders(OrderService $orderService)
    {
        try {
            $products = $orderService->getAllOrders();
            $response = ['data' => $products, 'success' => true, 'error' => false, 'message' => ''];
            $status = 200;
        } catch (\Exception $exception) {
            $response = ['data' => [], 'success' => false, 'error' => true, 'message' => $exception->getMessage()];
            $status = !empty($exception->status) ? $exception->status : 200;
        }
        return response()->json($response, $status);
    }

    /**
     * @param $id
     * @param OrderService $orderService
     * @return \Illuminate\Http\JsonResponse
     */
    public function getOrder($id, OrderService $orderService)
    {
        try {
            $products = $orderService->getOrderById($id);
            $response = ['data' => $products, 'success' => true, 'error' => false, 'message' => ''];
            $status = 200;
        } catch (\Exception $exception) {
            $response = ['data' => [], 'success' => false, 'error' => true, 'message' => $exception->getMessage()];
            $status = !empty($exception->status) ? $exception->status : 200;
        }
        return response()->json($response, $status);
    }

}