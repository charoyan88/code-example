<?php

namespace App\Http\Controllers;

use App\Services\CustomerService;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    /**
     * @param Request $request
     * @param CustomerService $customerService
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request, CustomerService $customerService)
    {
        try {
            $this->validate($request, [
                'phone_number' => 'required|unique:customers'
            ]);

            $data = $request->all();

            $group = $customerService->createCustomer($data);
            $response = ['data' => $group, 'success' => true, 'error' => false, 'message' => 'Customer successfully created!'];
            $status = 201;
        } catch (\Exception $exception) {
            $response = ['data' => [], 'success' => false, 'error' => true, 'message' => $exception->getMessage()];
            $status = !empty($exception->status) ? $exception->status : 200;
        }
        return response()->json($response, $status);
    }

    /**
     * @param CustomerService $customerService
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCustomers(CustomerService $customerService)
    {
        try {
            $products = $customerService->getAllCustomers();
            $response = ['data' => $products, 'success' => true, 'error' => false, 'message' => ''];
            $status = 200;
        } catch (\Exception $exception) {
            $response = ['data' => [], 'success' => false, 'error' => true, 'message' => $exception->getMessage()];
            $status = !empty($exception->status) ? $exception->status : 200;
        }
        return response()->json($response, $status);
    }

    /**
     * @param $id
     * @param CustomerService $customerService
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCustomer($id, CustomerService $customerService)
    {
        try {
            $products = $customerService->getCustomerById($id);
            $response = ['data' => $products, 'success' => true, 'error' => false, 'message' => ''];
            $status = 200;
        } catch (\Exception $exception) {
            $response = ['data' => [], 'success' => false, 'error' => true, 'message' => $exception->getMessage()];
            $status = !empty($exception->status) ? $exception->status : 200;
        }
        return response()->json($response, $status);
    }

}