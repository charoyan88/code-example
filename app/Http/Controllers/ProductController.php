<?php

namespace App\Http\Controllers;

use App\Product;
use App\Services\ProductService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    /**
     * @param ProductService $productService
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProducts(ProductService $productService)
    {
        try {
            $products = $productService->getAllProducts();
            $response = $products;
            $status = 200;
        }catch (\Exception $exception){
            $response = ['data' => [],'success' => false, 'error' => true, 'message' => $exception->getMessage()];
            $status = !empty($exception->status) ? $exception->status : 200;
        }
        return Response::json(['product'=>$response]);
    }

    /**
     * @param $id
     * @param ProductService $productService
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProduct($id, ProductService $productService)
    {
        try {
            $products = $productService->getProductById($id);
            $response = $products;
            $status = 200;
        }catch (\Exception $exception){
            $response = ['data' => [],'success' => false, 'error' => true, 'message' => $exception->getMessage()];
            $status = !empty($exception->status) ? $exception->status : 200;
        }
        return Response::json(['product'=>$response]);
    }

    public function create(Request $request, ProductService $productService)
    {
        try {
            Validator::make($request->all()['products'], [
                'name' => 'required:products',
                'price' => 'required:products'
            ])->validate();

            $data = $request->all();
            $product = $productService->createProduct($data['products']);
            $response = $productService->getProductById($product->id);

            $status = 201;
        } catch (\Exception $exception) {
            $response = ['data' => [], 'success' => false, 'error' => true, 'message' => $exception->getMessage()];
            $status = !empty($exception->status) ? $exception->status : 200;
        }
        return Response::json(['product'=>$response]);
    }

}