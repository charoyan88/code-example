<?php

namespace App\Contracts;

interface ProductServiceInterface
{
    public function getAllProducts();

    public function getProductById($id);
    public function createProduct($data);
}