<?php

namespace App\Contracts;

interface CustomerServiceInterface
{
    public function createCustomer($data);

    public function getAllCustomers();

    public function getCustomerById($id);
}