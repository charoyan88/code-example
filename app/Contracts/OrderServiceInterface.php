<?php

namespace App\Contracts;

interface OrderServiceInterface
{
    public function getAllOrders();

    public function getOrderById($id);

    public function createOrder($data);
}
