<?php

namespace App\Services;

use App\Contracts\ProductServiceInterface;
use App\Core\Services\BaseService;
use App\Repositories\ProductRepository;

class ProductService extends BaseService implements ProductServiceInterface
{
    /**
     * @var ProductRepository
     */
    private $productRepository;

    /**
     * ProductService constructor.
     * @param ProductRepository $productRepository
     */
    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAllProducts()
    {
        // TODO: Implement getAllProducts() method.
        return $this->productRepository->getAllProducts();

    }

    /**
     * @param $id
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null|static|static[]
     */
    public function getProductById($id)
    {
        // TODO: Implement getProductById() method.
        return $this->productRepository->getProductById($id);

    }

    /**
     * @param $data
     * @return $this|\Illuminate\Database\Eloquent\Model
     */
    public function createProduct($data)
    {
        // TODO: Implement createProduct() method.
        return $this->productRepository->createProduct($data);
    }

}