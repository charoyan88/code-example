<?php

namespace App\Services;


use App\Contracts\CustomerServiceInterface;
use App\Core\Services\BaseService;
use App\Repositories\CustomerRepository;

class CustomerService extends BaseService implements CustomerServiceInterface
{
    /**
     * @var CustomerRepository
     */
    private $customerRepository;

    /**
     * CustomerService constructor.
     * @param CustomerRepository $customerRepository
     */
    public function __construct(CustomerRepository $customerRepository)
    {
        $this->customerRepository = $customerRepository;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function createCustomer($data)
    {
        // TODO: Implement createCustomer() method.
        return $this->customerRepository->createCustomer($data);
    }

    /**
     * @return mixed
     */
    public function getAllCustomers()
    {
        // TODO: Implement getAllCustomers() method.
        return $this->customerRepository->getAllCustomers();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getCustomerById($id)
    {
        // TODO: Implement getCustomerById() method.
        return $this->customerRepository->getCustomerById($id);
    }

}