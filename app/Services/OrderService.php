<?php

namespace App\Services;

use App\Contracts\OrderServiceInterface;
use App\Core\Services\BaseService;
use App\Repositories\OrderRepository;


class OrderService extends BaseService implements OrderServiceInterface
{
    /**
     * @var OrderRepository
     */
    private $orderRepository;

    /**
     * OrderService constructor.
     * @param OrderRepository $orderRepository
     */
    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    /**
     * @param $data
     * @return $this|\Illuminate\Database\Eloquent\Model
     */
    public function createOrder($data)
    {
        // TODO: Implement createOrder() method.
        return $this->orderRepository->createOrder($data);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAllOrders()
    {
        // TODO: Implement getAllOrders() method.
        return $this->orderRepository->getAllOrders();
    }

    /**
     * @param $id
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null|static|static[]
     */
    public function getOrderById($id)
    {
        // TODO: Implement getOrderById() method.
        return $this->orderRepository->getOrderById($id);
    }

}