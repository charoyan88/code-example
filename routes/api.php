<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
$router->options(
    '/{any:.*}',
    [
        'middleware' => ['cors'],
        function (){
            return response(['status' => 'success']);
        }
    ]
);
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::resource('authenticate', 'AuthenticateController', ['only' => ['index']]);
Route::post('authenticate', 'AuthenticateController@authenticate');

Route::group(['middleware' => ['jwt.auth', 'jwt.refresh']], function () {
    Route::post('customers/create', 'CustomerController@create');
    Route::get('customers', 'CustomerController@getCustomers');
    Route::get('customer/{id}', 'CustomerController@getCustomer');

    Route::post('orders/create', 'OrderContorller@create');
    Route::get('orders', 'OrderController@getOrders');
    Route::get('order/{id}', 'OrderContorller@getOrder');


});

Route::get('products', ['middleware' => 'cors','uses' => 'ProductController@getProducts']);
Route::post('products', ['middleware' => 'cors','uses' => 'ProductController@create']);
Route::get('products/{id}', ['middleware' => 'cors','uses' => 'ProductController@getProduct']);